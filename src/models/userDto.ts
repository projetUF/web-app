export interface UserDto {
    id?: number;
    creation_date?: Date;
    modif_date?: Date;
    name?: string;
    firstname?: string;
    username?: string;
    mail?: string;
    password?: any;
    role_id?: number;
    date_of_birth?: Date;
    score?: number;
    phone?: string;
    address?: string;
    postal_code?: string;
    city?: string;
    country?: string;
}
