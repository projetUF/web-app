export interface SessionDto {
    id?: number;
    creation_date?: Date;
    modif_date?: Date;
    date_of_begin?: any;
    date_of_end?: any;
    date_end_register?: Date;
    active?: boolean;
}
