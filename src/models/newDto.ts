export interface NewDto {
    id?: number;
    creation_date?: Date;
    modif_date?: Date;
    date_of_begin?: Date;
    date_of_end?: Date;
    date_end_register?: Date;
    active?: boolean;
}
