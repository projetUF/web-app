import { Injectable } from '@angular/core';
import { UsersService } from './users.service';
import { UserDto } from '../models/userDto';
import { Md5 } from 'ts-md5';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  users: Array<UserDto> = [{}];
  loggedUser: UserDto = {};
  isLoggedIn: Boolean = false;
  roles: number;
  idUser: number;


  constructor(private usersService: UsersService) {

  }

  getAllUsers(): any {

  }


  signIn(user: UserDto): Boolean {
    let validUser: Boolean = false;

    this.usersService.getAllUsers().subscribe((res) => {
      Object(res).forEach((curUser) => {
        this.users.push(curUser);
      });
    });

    this.users.forEach((curUser) => {
      if (user.mail === curUser.mail && Md5.hashStr(user.password) === curUser.password) {
        validUser = true;
        this.loggedUser = curUser;
        this.isLoggedIn = true;
        this.roles = curUser.role_id;
        this.idUser = curUser.id;

        localStorage.setItem('loggedUser', JSON.stringify(curUser));
        localStorage.setItem('isLoggedIn', String(this.isLoggedIn));
      }
    });

    return validUser;
  }



}
