import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SessionDto } from '../models/sessionDto';

@Injectable({
  providedIn: 'root'
})
export class SessionsService {

  constructor(private http: HttpClient) { }

  getAllSessions(): Observable<SessionDto> {
    return this.http.get<SessionDto>('http://localhost:3000/sessions').pipe(map((res) => {
      console.log(res);
      return res;
    }));
  }
  getActiveSession(): Observable<SessionDto> {
    return this.http.get<SessionDto>('http://localhost:3000/sessions/active').pipe(map((res) => {
      return res;
    }));
  }
  getSessionById(id: number): Observable<SessionDto> {
    return this.http.get<SessionDto>(`http://localhost:3000/sessions/${id}`).pipe(map((res) => {
      console.log(res);
      return res;
    }));
  }
  updateSessionById(id: number, data: SessionDto): Observable<SessionDto> {
    return this.http.put<SessionDto>(`http://localhost:3000/sessions/${id}/update`, data).pipe(map((res) => {
      console.log(res);
      return res;
    }));
  }
  deleteSessionById(id: number): Observable<SessionDto> {
    return this.http.delete<SessionDto>(`http://localhost:3000/sessions/${id}/delete`).pipe(map((res) => {
      console.log(res);
      return res;
    }));
  }
  createSession(data: SessionDto): Observable<SessionDto> {
    return this.http.post<SessionDto>('http://localhost:3000/sessions/create', data).pipe(map((res) => {
      console.log(res);
      return res;
    }));
  }
}

