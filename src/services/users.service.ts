import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { stringify } from '@angular/compiler/src/util';
import { UserDto } from '../models/userDto';
import { Md5 } from 'ts-md5';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<UserDto> {
    return this.http.get<UserDto>('http://localhost:3000/users').pipe(map((res) => {
      return res;
    }));
  }
  getUserById(id: number): Observable<UserDto> {
    return this.http.get<UserDto>(`http://localhost:3000/users/${id}`).pipe(map((res) => {
      console.log(res);
      return res;
    }));
  }
  getRankingUsers(): Observable<UserDto> {
    return this.http.get<UserDto>(`http://localhost:3000/users/rank`).pipe(map((res) => {
      return res;
    }));
  }
  createUser(data: UserDto): Observable<UserDto> {
    data.password = Md5.hashStr(data.password);
    return this.http.post<UserDto>('http://localhost:3000/users/create', data).pipe(map((res) => {
      return res;
    }));
  }
  updateUser(data: UserDto, id: number): Observable<UserDto> {
    return this.http.put<UserDto>(`http://localhost:3000/users/${id}/update`, data).pipe(map((res) => {
      return res;
    }));
  }
  deleteUser(id: number): Observable<UserDto> {
    return this.http.delete<UserDto>(`http://localhost:3000/users/${id}/delete`).pipe(map((res) => {
      console.log(res);
      return res;
    }));
  }
}

