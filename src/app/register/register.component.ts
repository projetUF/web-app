import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDto } from '../../models/userDto';
import { AuthService } from '../../services/auth.service';
import { UsersService } from '../../services/users.service';
import { Md5 } from 'ts-md5/dist/md5';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  user: UserDto = {};
  userHash: UserDto = {};
  confirmPassword: any = '';
  userPassword: any = '';
  checkbox: Boolean = false;
  submitError: String = undefined;
  constructor(private usersService: UsersService, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {

  }

  onSubmit(): void {
    this.user.password = this.userPassword;
    if (this.user.date_of_birth == undefined || this.user.firstname == undefined || this.user.mail == undefined || this.user.name == undefined || this.user.password == undefined || this.user.phone == undefined || this.user.postal_code == undefined || this.user.username == undefined || this.checkbox == false) {
      this.submitError = "Tout les champs sont requis afin de valider le formulaire";
    }
    else if (this.user.password != this.confirmPassword) {
      this.submitError = "Merci d'entrer le même mot de passe";
    }
    else {
      this.user.score = 0;
      this.user.role_id = 1;
      this.userHash = this.user;
      this.usersService.createUser(this.userHash).subscribe((res) => { });
      this.router.navigate(['/login']);
    }
  }
}
