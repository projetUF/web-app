import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationDefiDetailsComponent } from './validation-defi-details.component';

describe('ValidationDefiDetailsComponent', () => {
  let component: ValidationDefiDetailsComponent;
  let fixture: ComponentFixture<ValidationDefiDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidationDefiDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationDefiDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
