import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordNotifComponent } from './change-password-notif.component';

describe('ChangePasswordNotifComponent', () => {
  let component: ChangePasswordNotifComponent;
  let fixture: ComponentFixture<ChangePasswordNotifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangePasswordNotifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordNotifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
