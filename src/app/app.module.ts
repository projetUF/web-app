import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatFormFieldControl } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from './users/users.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { AccueilComponent } from './accueil/accueil.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SessionRegisterComponent } from './session-register/session-register.component';
import { ConceptComponent } from './concept/concept.component';
import { AboutComponent } from './about/about.component';
import { ProfilComponent } from './profil/profil.component';
import { ClassementComponent } from './classement/classement.component';
import { ChangePasswordNotifComponent } from './change-password-notif/change-password-notif.component';
import { CardComponent } from './card/card.component';
import { ActualitesComponent } from './actualites/actualites.component';
import { DefisComponent } from './defis/defis.component';
import { AddSessionComponent } from './add-session/add-session.component';
import { ModifySessionComponent } from './modify-session/modify-session.component';
import { AddDefisComponent } from './add-defis/add-defis.component';
import { ModifyDefisComponent } from './modify-defis/modify-defis.component';
import { AddActuComponent } from './add-actu/add-actu.component';
import { ModifyActuComponent } from './modify-actu/modify-actu.component';
import { AccueilAdminComponent } from './accueil-admin/accueil-admin.component';
import { SessionAdminComponent } from './session-admin/session-admin.component';
import { SessionAdminDetailsComponent } from './session-admin-details/session-admin-details.component';
import { DefisAdminComponent } from './defis-admin/defis-admin.component';
import { ValidationDefiComponent } from './validation-defi/validation-defi.component';
import { ValidationDefiDetailsComponent } from './validation-defi-details/validation-defi-details.component';
import { ActuAdminComponent } from './actu-admin/actu-admin.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    NavbarComponent,
    FooterComponent,
    AccueilComponent,
    RegisterComponent,
    LoginComponent,
    ForgotPasswordComponent,
    SessionRegisterComponent,
    ConceptComponent,
    AboutComponent,
    ProfilComponent,
    ClassementComponent,
    ChangePasswordNotifComponent,
    CardComponent,
    ActualitesComponent,
    DefisComponent,
    AddSessionComponent,
    ModifySessionComponent,
    AddDefisComponent,
    ModifyDefisComponent,
    AddActuComponent,
    ModifyActuComponent,
    AccueilAdminComponent,
    SessionAdminComponent,
    SessionAdminDetailsComponent,
    DefisAdminComponent,
    ValidationDefiComponent,
    ValidationDefiDetailsComponent,
    ActuAdminComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
