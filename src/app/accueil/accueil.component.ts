import { Component, OnInit } from '@angular/core';
import { SessionsService } from '../../services/sessions.service';
import { SessionDto } from '../../models/sessionDto';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {

  session: SessionDto = {
    date_of_begin: undefined
  }
  constructor(private sessionsService: SessionsService, public authService: AuthService, private router: Router) { }

  ngOnInit(): void {

    // tslint:disable-next-line: deprecation
    this.sessionsService.getActiveSession().subscribe((res) => {
      this.session = {
        id: res[0].id,
        creation_date: res[0].creation_date,
        modif_date: res[0].modif_date,
        date_of_begin: res[0].date_of_begin,
        date_of_end: res[0].date_of_end,
        date_end_register: res[0].date_end_register,
        active: res[0].active
      };
    });
  }

  isLogin(): Boolean {
    let iAmLogin = JSON.parse(localStorage.getItem("isLoggedIn"));

    return iAmLogin;
  }

  disconnect(): void {
    localStorage.setItem('isLoggedIn', String(false));
    localStorage.setItem('loggedUser', String(undefined));

    this.router.navigate(['/']);
  }

}