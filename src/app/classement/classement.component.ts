import { Component, OnInit } from '@angular/core';
import { UserDto } from '../../models/userDto';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-classement',
  templateUrl: './classement.component.html',
  styleUrls: ['./classement.component.scss']
})
export class ClassementComponent implements OnInit {
  i: number = 0;
  users: Array<UserDto> = [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
  ]

  constructor(private usersService: UsersService) { }

  ngOnInit(): void {

    // tslint:disable-next-line: deprecation
    this.usersService.getRankingUsers().subscribe((res) => {
      console.log(res);
      for (this.i = 0; this.i < Object.keys(res).length; this.i++) {
        this.users[this.i] = res[this.i];
      }
    });
    console.log(this.users);
  }
}
