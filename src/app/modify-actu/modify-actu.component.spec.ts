import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyActuComponent } from './modify-actu.component';

describe('ModifyActuComponent', () => {
  let component: ModifyActuComponent;
  let fixture: ComponentFixture<ModifyActuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyActuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyActuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
