import { Component, OnInit } from '@angular/core';
import { SessionDto } from '../../models/sessionDto';
import { SessionsService } from '../../services/sessions.service';

@Component({
  selector: 'app-modify-session',
  templateUrl: './modify-session.component.html',
  styleUrls: ['./modify-session.component.scss']
})
export class ModifySessionComponent implements OnInit {
  session: SessionDto = {
    date_of_begin: undefined
  };
  dateDebut: String;
  dateFin: String;
  constructor(private sessionsService: SessionsService) { }

  ngOnInit(): void {
    this.sessionsService.getActiveSession().subscribe((res) => {
      this.session = {
        id: res[0].id,
        creation_date: res[0].creation_date,
        modif_date: res[0].modif_date,
        date_of_begin: res[0].date_of_begin,
        date_of_end: res[0].date_of_end,
        date_end_register: res[0].date_end_register,
        active: res[0].active
      };
    });
  }

  onSubmit(): void {
    this.session.date_of_begin = this.dateDebut;
    this.session.date_of_end = this.dateFin;
    this.sessionsService.updateSessionById(this.session.id, this.session).subscribe((res) => {

    });
  }

}
