import { Component, OnInit } from '@angular/core';
import { SessionsService } from '../../services/sessions.service';
import { SessionDto } from '../../models/sessionDto';

@Component({
  selector: 'app-session-register',
  templateUrl: './session-register.component.html',
  styleUrls: ['./session-register.component.scss']
})
export class SessionRegisterComponent implements OnInit {
  session: SessionDto;
  constructor(private sessionsService: SessionsService) { }

  ngOnInit(): void {

    // tslint:disable-next-line: deprecation
    this.sessionsService.getActiveSession().subscribe((res) => {
      this.session = {
        id: res[0].id,
        creation_date: res[0].creation_date,
        modif_date: res[0].modif_date,
        date_of_begin: res[0].date_of_begin,
        date_of_end: res[0].date_of_end,
        date_end_register: res[0].date_end_register,
        active: res[0].active
      };
    });
  }

}
