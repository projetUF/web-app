import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDto } from '../../models/userDto';
import { AuthService } from '../../services/auth.service';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: UserDto = {
    mail: undefined,
    password: undefined
  };
  errorLogin: String = undefined;
  constructor(
    private authService: AuthService,
    private router: Router,
    private usersService: UsersService) { }

  ngOnInit(): void {
  }

  onLoggedIn(): void {
    let isValidUser: Boolean = this.authService.signIn(this.user);
    if (isValidUser) {
      this.errorLogin = undefined;
      this.router.navigate(['/']);
    }
    else {
      this.errorLogin = "L'e-mail et/ou le mot de passe sont incorrect";
    }
  }

}
