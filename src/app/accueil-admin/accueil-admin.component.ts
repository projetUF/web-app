import { Component, OnInit } from '@angular/core';
import { SessionDto } from '../../models/sessionDto';
import { SessionsService } from '../../services/sessions.service';

@Component({
  selector: 'app-accueil-admin',
  templateUrl: './accueil-admin.component.html',
  styleUrls: ['./accueil-admin.component.scss']
})
export class AccueilAdminComponent implements OnInit {
  session: SessionDto = {
    date_of_begin: undefined
  };
  constructor(private sessionsService: SessionsService) { }

  ngOnInit(): void {
    // tslint:disable-next-line: deprecation
    this.sessionsService.getActiveSession().subscribe((res) => {
      this.session = {
        id: res[0].id,
        creation_date: res[0].creation_date,
        modif_date: res[0].modif_date,
        date_of_begin: res[0].date_of_begin,
        date_of_end: res[0].date_of_end,
        date_end_register: res[0].date_end_register,
        active: res[0].active
      };
    });
  }

}
