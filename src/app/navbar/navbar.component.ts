import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  toggle: boolean = true;
  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  isLogin(): Boolean {
    let iAmLogin = JSON.parse(localStorage.getItem("isLoggedIn"));

    return iAmLogin;
  }
  disconnect(): void {
    localStorage.setItem('isLoggedIn', String(false));
    localStorage.setItem('loggedUser', String(undefined));

    this.router.navigate(['/']);
  }
}
