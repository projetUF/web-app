import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyDefisComponent } from './modify-defis.component';

describe('ModifyDefisComponent', () => {
  let component: ModifyDefisComponent;
  let fixture: ComponentFixture<ModifyDefisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyDefisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyDefisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
