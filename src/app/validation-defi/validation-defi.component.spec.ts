import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationDefiComponent } from './validation-defi.component';

describe('ValidationDefiComponent', () => {
  let component: ValidationDefiComponent;
  let fixture: ComponentFixture<ValidationDefiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidationDefiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationDefiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
