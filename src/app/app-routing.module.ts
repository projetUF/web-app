import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ChangePasswordNotifComponent } from './change-password-notif/change-password-notif.component';
import { ClassementComponent } from './classement/classement.component';
import { ActualitesComponent } from './actualites/actualites.component';
import { CardComponent } from './card/card.component';
import { ConceptComponent } from './concept/concept.component';
import { DefisComponent } from './defis/defis.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { ProfilComponent } from './profil/profil.component';
import { RegisterComponent } from './register/register.component';
import { SessionRegisterComponent } from './session-register/session-register.component';
import { UsersComponent } from './users/users.component';
import { AddSessionComponent } from './add-session/add-session.component';
import { ModifySessionComponent } from './modify-session/modify-session.component';
import { AddDefisComponent } from './add-defis/add-defis.component';
import { ModifyDefisComponent } from './modify-defis/modify-defis.component';
import { ModifyActuComponent } from './modify-actu/modify-actu.component';
import { AddActuComponent } from './add-actu/add-actu.component';
import { AccueilAdminComponent } from './accueil-admin/accueil-admin.component';
import { SessionAdminComponent } from './session-admin/session-admin.component';
import { SessionAdminDetailsComponent } from './session-admin-details/session-admin-details.component';
import { DefisAdminComponent } from './defis-admin/defis-admin.component';
import { ValidationDefiComponent } from './validation-defi/validation-defi.component';
import { ValidationDefiDetailsComponent } from './validation-defi-details/validation-defi-details.component';
import { ActuAdminComponent } from './actu-admin/actu-admin.component';

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'home', component: AccueilComponent },
  { path: 'users', component: UsersComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'forgot', component: ForgotPasswordComponent },
  { path: 'session-register', component: SessionRegisterComponent },
  { path: 'concept', component: ConceptComponent },
  { path: 'about', component: AboutComponent },
  { path: 'profil', component: ProfilComponent },
  { path: 'classement', component: ClassementComponent },
  { path: 'change', component: ChangePasswordNotifComponent },
  { path: 'card', component: CardComponent },
  { path: 'actualites', component: ActualitesComponent },
  { path: 'defis', component: DefisComponent },
  { path: 'add-session', component: AddSessionComponent },
  { path: 'modify-session', component: ModifySessionComponent },
  { path: 'add-defis', component: AddDefisComponent },
  { path: 'modify-defis', component: ModifyDefisComponent },
  { path: 'modify-actu', component: ModifyActuComponent },
  { path: 'add-actu', component: AddActuComponent },
  { path: 'accueil-admin', component: AccueilAdminComponent },
  { path: 'session-admin', component: SessionAdminComponent },
  { path: 'session-admin-details', component: SessionAdminDetailsComponent },
  { path: 'defis-admin', component: DefisAdminComponent },
  { path: 'validation-defi', component: ValidationDefiComponent },
  { path: 'validation-defi-details', component: ValidationDefiDetailsComponent },
  { path: 'actu-admin', component: ActuAdminComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
