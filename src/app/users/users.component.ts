import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { UserDto } from '../../models/userDto';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  user: UserDto;

  constructor(private usersService: UsersService) { }

  ngOnInit(): void {
    // tslint:disable-next-line: max-line-length
    this.user = { name: '', firstname: '', username: '', mail: '', password: '', role_id: 1, date_of_birth: undefined, score: 0, phone: '', address: '', postal_code: '', city: '', country: '' };

    // tslint:disable-next-line: deprecation
    this.usersService.getAllUsers().subscribe((res) => {
      console.log(res);
    });
  }

  onSubmit(): void {
    console.log(this.user);
    // tslint:disable-next-line: deprecation
    this.usersService.createUser(this.user).subscribe((res) => {
      console.log(res);
    });
  }

}
