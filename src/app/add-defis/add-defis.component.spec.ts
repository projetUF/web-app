import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDefisComponent } from './add-defis.component';

describe('AddDefisComponent', () => {
  let component: AddDefisComponent;
  let fixture: ComponentFixture<AddDefisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDefisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDefisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
