import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionAdminDetailsComponent } from './session-admin-details.component';

describe('SessionAdminDetailsComponent', () => {
  let component: SessionAdminDetailsComponent;
  let fixture: ComponentFixture<SessionAdminDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionAdminDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionAdminDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
