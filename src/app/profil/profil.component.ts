import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDto } from '../../models/userDto';
import { AuthService } from '../../services/auth.service';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {
  check: boolean = false;
  loggedUser: UserDto;
  constructor(private usersService: UsersService, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.loggedUser = JSON.parse(localStorage.getItem('loggedUser'));


  }


  onClick(): void {
    this.check = !this.check;
  }

  onSubmit(): void {
    this.usersService.updateUser(this.loggedUser, this.loggedUser.id).subscribe((res) => {
    });
  }

}
